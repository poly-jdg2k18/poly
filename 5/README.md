# Launch server
1. Open terminal
2. Navigate to this directory.
3. Launch `PHPPortable\App\Php\php.exe -S localhost:8000`. Keep it opened. You should see something like this:
```
C:\Temp\poly-cool\jdg-2018\5>PHPPortable\App\Php\php.exe -S localhost:8000
PHP 5.6.4 Development Server started at Fri Jan 05 11:06:46 2018
Listening on http://localhost:8000
Document root is C:\Temp\poly-cool\jdg-2018\5
Press Ctrl-C to quit.
```
4. Navigate to http://localhost:8000

# Using the API
Requests have to be of type application/json