<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// You can find a PHP algorithm to permutate in recipe 4.26 of O'Reilly's "PHP Cookbook".
// https://stackoverflow.com/questions/5506888/permutations-all-possible-sets-of-numbers
function pc_permute($items, $perms = array( )) {
    if (empty($items)) {
        $return = array($perms);
    }  else {
        $return = array();
        for ($i = count($items) - 1; $i >= 0; --$i) {
             $newitems = $items;
             $newperms = $perms;
         list($foo) = array_splice($newitems, $i, 1);
             array_unshift($newperms, $foo);
             $return = array_merge($return, pc_permute($newitems, $newperms));
         }
    }
    return $return;
}

require_once __DIR__.'/vendor/autoload.php';

$app = new Silex\Application();
$app['debug'] = true;

$app->post('/permutations', function() use($app) {
    $input = json_decode(file_get_contents('php://input'));
    $elements = explode(' ', preg_replace('/\s+/', ' ', $input->input));
    $res = pc_permute($elements);

    foreach ($res as $k => $set) {
    	$res[$k] = implode(' ', $set);
    }

	header('Content-Type: application/json');
    return json_encode(['result' => array_values(array_unique($res))]);
});

$app->run();